Source: mu-editor
Section: python
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Nick Morrott <nickm@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 black,
 dh-python,
 fonts-inconsolata,
 pandoc,
 python3 (>= 3.6),
 python3-appdirs (>= 1.4.3),
 python3-flask (>= 1.0.2),
 python3-mock,
 python3-nudatus,
 python3-pycodestyle (>= 2.4.0),
 python3-pyflakes (>= 2.0.0),
 python3-pyqt5 (>= 5.11.3),
 python3-pyqt5.qsci (>= 2.10.4),
 python3-pyqt5.qtchart,
 python3-pyqt5.qtserialport (>= 5.11.3),
 python3-pytest,
 python3-pytest-random-order,
 python3-pytest-xvfb,
 python3-qtconsole (>= 4.3.1),
 python3-requests,
 python3-semver,
 python3-serial (>= 3.4),
 python3-setuptools,
 python3-sphinx,
 python3-tk,
 python3-uflash,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://codewith.mu/
Vcs-Browser: https://salsa.debian.org/python-team/applications/mu-editor
Vcs-Git: https://salsa.debian.org/python-team/applications/mu-editor.git

Package: mu-editor
Architecture: all
Depends:
 black,
 fonts-inconsolata,
 libjs-normalize.css,
 python3 (>= 3.6),
 python3-flask,
 python3-guizero,
 python3-pyqt5,
 python3-pyqt5.qsci,
 python3-pyqt5.qtchart,
 python3-pyqt5.qtserialport,
 python3-qtconsole,
 python3-uflash,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 mu-editor-doc,
 python3-gpiozero,
 python3-pigpio,
Description: simple editor for beginner Python programmers
 Mu is a simple code editor for beginner programmers, based on extensive
 feedback from teachers and learners. Having said that, Mu is for anyone
 who wants to use a simple "no frills" editor.
 .
 Mu is a modal editor with modes for:
 .
  * standard Python 3 (including a graphical debugger)
  * the micro:bit's version of MicroPython
  * Adafruit's CircuitPython
  * Pygame Zero
 .
 Some modes provide read-eval-print loop (REPL) support, either running on
 a connected CircuitPython or MicroPython device, or as a Jupyter-based
 iPython session in Python3 mode.
 .
 This package contains the Mu editor. Detailed online user documentation
 and tutorials for the Mu editor are available from within the editor,
 or at https://codewith.mu

Package: mu-editor-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 www-browser,
Description: simple editor for beginner Python programmers (documentation)
 Mu is a simple code editor for beginner programmers, based on extensive
 feedback from teachers and learners. Having said that, Mu is for anyone
 who wants to use a simple "no frills" editor.
 .
 Mu is a modal editor with modes for:
 .
  * standard Python 3 (including a graphical debugger)
  * the micro:bit's version of MicroPython
  * Adafruit's CircuitPython
  * Pygame Zero
 .
 Some modes provide read-eval-print loop (REPL) support, either running on
 a connected CircuitPython or MicroPython device, or as a Jupyter-based
 iPython session in Python3 mode.
 .
 This package contains the developer documentation for the Mu editor. Detailed
 user documentation and tutorials for the Mu editor are available from within
 the editor, or at https://codewith.mu
